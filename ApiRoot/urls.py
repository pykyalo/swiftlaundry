from django.conf.urls import url, include

# from api_root.views import RootView
from Auth.urls import urlpatterns as auth_paths
from Outlet.urls import urlpatterns as outlet_paths
from Location.urls import urlpatterns as location_paths
from Order.urls import urlpatterns as order_paths
from Stockrecord.urls import urlpatterns as stockrecord_paths
from Payment.urls import urlpatterns as payment_paths

from root_view import RootView

urlpatterns = [
    url(r'^$', RootView.as_view(), name='root_view'),
] + auth_paths + \
    outlet_paths + \
    location_paths + \
    order_paths + \
    stockrecord_paths + \
    payment_paths
