from django.contrib import admin

#--------------------------------------------------------------------#
from Auth.models import WorkTitle, Profile, LoginToken


class LoginTokenAdmin(admin.ModelAdmin):
    list_display = ('phonenumber', 'token')
admin.site.register(LoginToken, LoginTokenAdmin)


class WorkTitleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']

admin.site.register(WorkTitle, WorkTitleAdmin)


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('phonenumber', 'user', 'title', 'is_active', 'date_created')
    search_fields = ['phonenumber', 'id']

admin.site.register(Profile, ProfileAdmin)
