# Generated by Django 2.0.7 on 2018-07-20 05:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Auth', '0003_auto_20180719_0651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='national_id',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
    ]
