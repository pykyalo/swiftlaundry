#------------------------------------------------------------------#
from django.db import models
from django.contrib.auth.models import User


class LoginToken(models.Model):
    token = models.TextField()
    phonenumber = models.CharField(max_length=15)
    is_used = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    used_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.token


class WorkTitle(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phonenumber = models.CharField(max_length=10, unique=True)
    phone_local = models.CharField(max_length=4) #country code
    national_id = models.CharField(max_length=15, null=True, blank=True)
    internal_id = models.CharField(max_length=15, null=True, blank=True)
    title = models.ForeignKey(WorkTitle, on_delete=models.CASCADE, null=True)
    is_outlet_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}, {}".format(self.user.first_name, self.phonenumber)
