from rest_framework.serializers import ModelSerializer

#-------------------------------------------------------#
from .models import WorkTitle, Profile
from django.contrib.auth.models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'username'
        )


class WorkTitleSerializer(ModelSerializer):
    class Meta:
        model = WorkTitle
        fields = ('id', 'name')


class LeanProfileSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'user',
            'phonenumber',
            'phone_local',
            'national_id',
            'internal_id',
            'title',
            'is_outlet_admin',
            'is_active'
        )


class ProfileSerializer(ModelSerializer):

    user = UserSerializer()
    title = WorkTitleSerializer()

    class Meta:
        model = Profile
        fields = (
            'user',
            'phonenumber',
            'phone_local',
            'national_id',
            'internal_id',
            'title',
            'is_outlet_admin',
            'is_active',
            'date_created',
            'last_updated'
        )
