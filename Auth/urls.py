from django.conf.urls import url, include


from .views import (
    UserSignUpView,
    OutletStaffView,
    OutletStaffDetailsView,
    GetLoginToken,
    LoginView,
    WorkTitleView
)

urlpatterns = [
    url(r'^getlogintoken', GetLoginToken.as_view(), name='getlogintoken'),
    url(r'^login', LoginView.as_view(), name='login'),
    url(r'^signup', UserSignUpView.as_view(), name='signup'),
    url(r'^outletstaff/$', OutletStaffView.as_view(), name='outletstaff'),
    url(r'^outletstaff/(?P<pk>\d+)/$', OutletStaffDetailsView.as_view(), name='outletstaff-details'),
    url(r'^titles', WorkTitleView.as_view(), name='titles')
]
