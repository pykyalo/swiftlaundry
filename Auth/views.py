#-----------------------------------------------------------#
from django.shortcuts import render
from django.contrib.auth.models import User

#----------------------------------------------------------#
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

#-----------------------------------------------------------#
from Auth.models import Profile, LoginToken, WorkTitle
from Outlet.models import Outlet, OutletStaff

#------------------------------------------------------------#
from .serializers import (
    UserSerializer,
    LeanProfileSerializer,
    ProfileSerializer,
    WorkTitleSerializer
)

#-----------------------------------------------------------#
from Outlet.serializers import (
    OutletStaffSerializer,
    LeanOutletStaffSerializer,
    LeanOutletSerializer
)

#-----------------------------------------------------------#
from Communication.utils import create_message

#-----------------------------------------------------------#
import base64
import hashlib
from .utils import generate_token


class WorkTitleView(APIView):
    """
        Description: Returns laundry shops work titles
    """
    def get(self, request, format=None):
        titles = WorkTitle.objects.all()
        titles_serializer = WorkTitleSerializer(titles, many=True)
        return Response(titles_serializer.data, status=status.HTTP_200_OK)


class GetLoginToken(APIView):
    """
        Description: Returns a 6 digit string. The string is sent to user's
        phonenumber.
        \nPOST\n
        {
            "phonenumber": "0712345678",
        }
    """

    def post(self, request, format=None):
        phonenumber = request.data['phonenumber']
        profile_qs  = Profile.objects.filter(phonenumber=phonenumber)

        if not profile_qs.exists():
            error = {
                'error': '{} is NOT registered.'.format(phonenumber)
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        if not profile_qs.first().is_active:
            error = {
                'error': 'Sorry, your account has been suspended by your shop admin'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        token = generate_token()
        utf_token = token.encode("utf-8")
        hashed_token = base64.b64encode(utf_token)
        hashed_token = hashlib.sha512(hashed_token)
        digest = hashed_token.hexdigest()

        token_obj = LoginToken.objects.create(
            token=digest,
            phonenumber=phonenumber
        )
        token_obj.save()

        # SMS details
        first_token_part, second_token_part = self.splittoken(token)
        token = first_token_part + ' ' + second_token_part
        body = "{} is your login token.".format(token)
        sent_to = '254' + phonenumber

        create_message(sent_to, 'LOGIN', body)
        return Response(status=status.HTTP_201_CREATED)

    def splittoken(self, token):
        split = -((-len(token))//2)
        return token[:split], token[split:]


class LoginView(APIView):
    """
        Description: Swift Clean API uses Token Based Authentication with one
        off generated 6 digit tokens\n
        POST:
        {
        "phonenumber": "0712345678",
        "login_token": "111 222"
        }
    """
    def post(self, request, format=None):
        # Verify token

        login_token = request.data['login_token']
        phonenumber = request.data['phonenumber']

        # Verify phonenumber

        if not Profile.objects.filter(phonenumber=phonenumber).exists():
            error = {
                'error': 'Sorry, {} is not registered.'.format(phonenumber)
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        utf_token = login_token.encode("utf-8")
        hashed_token = base64.b64encode(utf_token)
        hashed_token = hashlib.sha512(hashed_token)
        digest = hashed_token.hexdigest()

        token_queryset = LoginToken.objects.filter(
            token=digest,
            phonenumber=phonenumber,
            is_used=False
        )

        if token_queryset:
            token_obj = token_queryset.first()
            token_obj.is_used = True
            token_obj.save()

            profile = Profile.objects.get(phonenumber=phonenumber)
            user = User.objects.get(id=profile.user_id)
            token = Token.objects.get(user_id=user.id)
            profile_serializer = ProfileSerializer(profile)

            outlet_staff = OutletStaff.objects.filter(staff_id=profile.id)
            content = {
                'token': token.key
            }

            if outlet_staff.exists():
                # outlet = Outlet.objects.get(id=outlet_staff.first().outlet.id)
                outlet_serializer = OutletStaffSerializer(outlet_staff.first())
                content.update(outlet_serializer.data)
                return Response(content, status=status.HTTP_200_OK)

            error = {
                'error': 'Sorry, profile is incomplete. Please ask your shop admin to update your details.'
            }

            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        error = {
            'error': 'Sorry, {} is invalid. Please try again.'.format(login_token)
        }
        return Response(error, status=status.HTTP_400_BAD_REQUEST)


class OutletStaffDetailsView(APIView):
    """
        Description: Returns a shop staff details \n

        PUT:
        \n
            {
            firstName: "Joe",
            lastName: "William",
            email: "joewilliam@gmail.com",
            phonenumber: "0712345678",
            national_id: "",
            internal_id: "",
            is_active: 1,
            }

    """

    permission_classes = (IsAuthenticated, )

    def put(self, request, pk, format=None):
        user = request.user

        first_name = request.data['first_name']
        last_name = request.data['last_name']
        email = request.data['email']
        phonenumber = request.data['phonenumber']
        phone_local = request.data['phone_local']
        national_id = request.data['national_id']
        internal_id = request.data['internal_id']
        is_active = request.data['is_active']

        staff = OutletStaff.objects.get(id=pk)

        profile = Profile.objects.get(id=staff.staff.id)

        # admin_profile = Profile.objects.get(user_id=user.id, is_outlet_admin=True)
        try:
            admin_profile = Profile.objects.get(user_id=user.id, is_outlet_admin=True)
        except:
            error = {
                'error': 'Sorry {}, You do not have Swift Verified Rights to edit staff'.format(user.first_name)
            }

            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        existing_user_qs = User.objects.filter(email=email)

        if existing_user_qs.exists() and profile.user.email != email:

            error =  {
                'error': '{}  is already taken.'.format(email)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if Profile.objects.filter(phonenumber=phonenumber).exists() and profile.phonenumber != phonenumber:
            error = {
                'error': '{} is already taken.'.format(phonenumber)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        outlet_queryset = Outlet.objects.filter(admin_id=admin_profile.id)

        if outlet_queryset:
            outlet = outlet_queryset.first()

            # Create user, profile and shop
            updated_user = self.update_user(profile,first_name, last_name, email)

            new_profile = self.update_profile(
                phone_local,
                phonenumber,
                is_active,
                national_id,
                internal_id,
                updated_user.id
            )

            outlet_staff_obj = OutletStaff.objects.get(staff_id=new_profile.id, outlet_id=outlet.id)

            data = {
                'outlet': outlet.id,
                'staff': new_profile.id
            }

            outlet_staff_serializer = LeanOutletStaffSerializer(outlet_staff_obj, data=data, partial=True)

            if outlet_staff_serializer.is_valid():
                outlet_staff = outlet_staff_serializer.save()
                response_content_serializer = OutletStaffSerializer(outlet_staff)

                sent_to = phone_local + phonenumber
                body = ('{}, your details on {}, on Swift Clean have been changed. '
                'Your Account'
                ' was updated by {}').format(updated_user.first_name, outlet.name, user.first_name)
                create_message(sent_to, 'AC', body)

                return Response(response_content_serializer.data, status=status.HTTP_200_OK)

            return Response(outlet_staff_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        error = {
            'error': 'Sorry, You have not added your laundry services to Swift Clean.'
        }
        return Response(error, status=status.HTTP_400_BAD_REQUEST)



    def update_user(self, profile, first_name, last_name, email):
        user_obj = User.objects.get(email=profile.user.email)

        user_data = {
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "username": email
        }

        user_serializer = UserSerializer(user_obj, data=user_data, partial=True)

        if user_serializer.is_valid():
            user = user_serializer.save()
            return user

    def update_profile(self, phone_local, phonenumber, is_active, national_id, internal_id, user):
        profile = Profile.objects.get(user_id=user)

        if profile.is_outlet_admin:
            profile_data = {
                'phone_local': phone_local,
                'phonenumber': phonenumber,
                'user': user,
                'national_id': national_id,
                'internal_id': internal_id,
                'is_active': is_active,
                'is_outlet_admin': True
            }
            profile_serializer = LeanProfileSerializer(profile, data=profile_data, partial=True)

            if profile_serializer.is_valid():
                profile = profile_serializer.save()
                return profile
        else:
            profile_data = {
                'phone_local': phone_local,
                'phonenumber': phonenumber,
                'user': user,
                'national_id': national_id,
                'internal_id': internal_id,
                'is_active': is_active,
                'is_outlet_admin': False
            }
            profile_serializer = LeanProfileSerializer(profile, data=profile_data, partial=True)

            if profile_serializer.is_valid():
                profile = profile_serializer.save()
                return profile


class OutletStaffView(APIView):
    """
        Description: A Shop Admin can add a worker to to their outlet. \n

        POST
        \n
            {
                "first_name": "Joe",
                "last_name": "William",
                "email": "joewilliam@gmail.com",
                "phonenumber": "0712345678",
                "phone_local": "254",
                "national_id": "",
                "internal_id": "",
                "title": 12,
                "outlet": 1
            }

    """

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        """
            returns profiles associated with a shop in a shop
        """

        user = request.user
        profile = Profile.objects.get(user_id=user.id)

        if profile.is_outlet_admin:
            outlet = Outlet.objects.get(admin_id=profile.id)
            outlet_staff = OutletStaff.objects.filter(outlet_id=outlet.id)
            outlet_staff_serializer = OutletStaffSerializer(outlet_staff, many=True)
            return Response(
                outlet_staff_serializer.data,
                status=status.HTTP_200_OK
            )

        outlet_staff = OutletStaff.objects.filter(staff_id=profile.id).first()
        outlet_staff_serializer = OutletStaffSerializer(outlet_staff)
        return Response(outlet_staff_serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):

        user = request.user

        first_name = request.data['first_name']
        last_name = request.data['last_name']
        email = request.data['email']
        phonenumber = request.data['phonenumber']
        phone_local = request.data['phone_local']
        national_id = request.data['national_id']
        internal_id = request.data['internal_id']
        title = request.data['title']

        # To add a staff to your a shop, we need to verify the owner as Admin
        try:
            profile = Profile.objects.get(user_id=user.id, is_outlet_admin=True)
        except:
            error = {
                'error': 'Sorry {}, You do not have Swift Verified Rights to add new staff'.format(user.first_name)
            }

            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        # users are Identified by there emails and phonenumber
        if User.objects.filter(email=email).exists():
            error =  {
                'error': 'Account {}  already exists.'.format(email)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if Profile.objects.filter(phonenumber=phonenumber).exists():
            error = {
                'error': 'Profile {} already exists'.format(phonenumber)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        outlet_queryset = Outlet.objects.filter(admin_id=profile.id)

        if outlet_queryset:
            outlet = outlet_queryset.first()

            # Create user, profile and shop
            new_user = self.create_user(first_name, last_name, email)

            new_profile = self.create_profile(
                phone_local,
                phonenumber,
                title,
                national_id,
                internal_id,
                new_user.id
            )

            data = {
                'outlet': outlet.id,
                'staff': new_profile.id
            }

            outlet_staff_serializer = LeanOutletStaffSerializer(data=data)

            if outlet_staff_serializer.is_valid():
                outlet_staff = outlet_staff_serializer.save()
                response_content_serializer = OutletStaffSerializer(outlet_staff)

                # create and send sms to admin
                sent_to = phone_local + phonenumber
                body = ('{}, welcome to {}, on Swift Clean. We are very happy to have'
                ' you onboard. Your Account'
                ' was created by {}').format(new_user.first_name, outlet.name, user.first_name)
                create_message(sent_to, 'AC', body)

                return Response(response_content_serializer.data, status=status.HTTP_200_OK)

            return Response(outlet_staff_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        error = {
            'error': 'Sorry, You have not added your laundry services to Swift Clean.'
        }
        return Response(error, status=status.HTTP_400_BAD_REQUEST)



    def create_user(self, first_name, last_name, email):
        user_data = {
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "username": email
        }

        user_serializer = UserSerializer(data=user_data)

        if user_serializer.is_valid():
            user = user_serializer.save()
            Token.objects.create(user=user) # Create Authentication Token
            return user

    def create_profile(self, phone_local, phonenumber, title, national_id, internal_id, user):
        profile_data = {
            'phone_local': phone_local,
            'phonenumber': phonenumber,
            'user': user,
            'title': title,
            'national_id': national_id,
            'internal_id': internal_id,
            'is_active': True,
            'is_outlet_admin': False
        }

        profile_serializer = LeanProfileSerializer(data=profile_data)

        if profile_serializer.is_valid():
            profile = profile_serializer.save()
            return profile


class UserSignUpView(APIView):

    """
        Description: Swift has 3 types of customers: \t{shop admins, normal shop users, customers visiting shops}\n
        All shop workers are NOT shop admins by default. \note
        To be come a shop admin,  the swift system owners upgrade one user per shop to have shop adminstration rightsself.
        #This is done manually to verify shops and prevent illegal signupsself.\n

        To create an ADMIN account:
        \n post
        \n {
            "first_name": "Joe",
            "last_name": "William",
            "email": "joewilliam@gmail.com",
            "phonenumber": "0712345678",
            "phone_local": "254",
            "national_id": "",
            "title": 12
        }

        \nSample response\n
        {
        "user": {
        "first_name": "Joe",
        "last_name": "William",
        "email": "joewilliam@gmail.com",
        "username": "joewilliam@gmail.com"
        },
        "phonenumber": "0724230064",
        "phone_local": "254",
        "national_id": "",
        "title": {
        "id": 1,
        "name": "Shop Admin"
        },
        "is_outlet_admin": false,
        "is_active": true,
        "date_created": "2018-07-20T06:00:58.293693Z",
        "last_updated": "2018-07-20T06:00:58.293813Z"
        }

    """

    def post(self, request, format=None):
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        email = request.data['email']
        phonenumber = request.data['phonenumber']
        phone_local = request.data['phone_local']
        national_id = request.data['national_id']
        title = request.data['title']

        # users are Identified by there emails and phonenumber
        if User.objects.filter(email=email).exists():
            error =  {
                'error': 'Account {}  already exists.'.format(email)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if Profile.objects.filter(phonenumber=phonenumber).exists():
            error = {
                'error': 'Profile {} already exists'.format(phonenumber)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        user_token_obj = self.create_user(first_name, last_name, email)

        profile = self.create_profile(
            phone_local,
            phonenumber,
            title,
            national_id,
            user_token_obj['user'].pk
        )
        profile_serializer = ProfileSerializer(profile)

        content_obj = {'Token': user_token_obj['token'].key}

        # profile_serializer.data is immutable so we update the new dictionary
        content_obj.update(profile_serializer.data)

        # create and send sms to admin
        sent_to = phone_local + phonenumber
        body = '{}, welcome to Swift Clean. We are very happy to have you on board.'.format(user_token_obj['user'].first_name)
        create_message(sent_to, 'AC', body)

        return Response(content_obj, status=status.HTTP_200_OK)


    def create_user(self, first_name, last_name, email):
        user_data = {
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "username": email
        }

        user_serializer = UserSerializer(data=user_data)

        if user_serializer.is_valid():
            user = user_serializer.save()
            token = Token.objects.create(user=user) # Create Authentication Token
            user_token = {'user': user, 'token': token}
            return user_token

    def create_profile(self, phone_local, phonenumber, title, national_id, user):
        profile_data = {
            'phone_local': phone_local,
            'phonenumber': phonenumber,
            'user': user,
            'title': title,
            'national_id': national_id,
            'is_active': True,
            'is_outlet_admin': True
        }

        profile_serializer = LeanProfileSerializer(data=profile_data)

        if profile_serializer.is_valid():
            profile = profile_serializer.save()
            return profile
