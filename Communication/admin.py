from django.contrib import admin

#-----------------------------------------------------#
from Communication.models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sent_to', 'category', 'body', 'date_sent')
    search_fields = ['sent_to', 'body']

admin.site.register(Message, MessageAdmin)
