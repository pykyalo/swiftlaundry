from django.apps import AppConfig


class CommunicationConfig(AppConfig):
    name = 'Communication'

    def ready(self):
        import Communication.signals
