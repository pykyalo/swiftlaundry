from django.db import models


class Message(models.Model):

    MESSAGE_CATEGORIES = (
        ('LOGIN', 'Login'),
        ('AC', 'Account Created'),
        ('GENERAL', 'General'),
        ('PAYMENT', 'Payment')
    )

    sent_to = models.CharField(max_length=100)
    body = models.TextField()
    category = models.CharField(max_length=10, choices=MESSAGE_CATEGORIES)
    date_sent = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.sent_to
