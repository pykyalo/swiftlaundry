from django.db.models.signals import post_save
from django.dispatch import receiver

#---------------------------------------------------#
from .models import Message

#--------------------------------------------------#
from .AfricasTalkingGateway import (
    AfricasTalkingGateway, AfricasTalkingGatewayException
)


@receiver(post_save, sender=Message)
def create_message(sender, instance, created, **kwargs):
    if created:
        username = 'swiftclean'
        apikey = '90c6b55fabdd75bd48b7e27d75c0c12fbbea2419d2f5c56a8f6448c11ff0e55a'

        to = "%s," % (instance.sent_to)
        gateway = AfricasTalkingGateway(username, apikey)

        try:
            results = gateway.sendMessage(to, instance.body)
            return "SMS sent"

        except AfricasTalkingGatewayException as e:
            error = {
                "error_at": str(e)
            }
            return error
