from .models import Message

def create_message(sent_to, category, body):
    message = Message.objects.create(
        sent_to=sent_to,
        category=category,
        body=body
    )

    message.save()
