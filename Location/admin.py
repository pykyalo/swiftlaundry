from django.contrib import admin
from Location.models import Country, County, City


class CountryAdmin(admin.ModelAdmin):
    list_display = (
        'code',
        'name'
    )
    search_fields = ['code', 'name']
admin.site.register(Country, CountryAdmin)


class CountyAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'country'
    )
    search_fields = ['name', 'id']
admin.site.register(County, CountyAdmin)


class CityAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'country',
        'county'
    )
    search_fields = ['name', 'id']
admin.site.register(City, CityAdmin)
