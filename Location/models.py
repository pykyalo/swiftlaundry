from django.db import models

class Country(models.Model):
    class Meta:
        verbose_name_plural = 'Countries'

    name = models.CharField(max_length=50, default='Kenya')
    code = models.CharField(max_length=20, default='254')

    def __str__(self):
        return self.name

class County(models.Model):
    class Meta:
        verbose_name_plural = 'Counties'

    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, default='Nairobi')

    def __str__(self):
        return "Country: {}, Name: {}".format(self.country.name, self.name)

class City(models.Model):
    class Meta:
        verbose_name_plural = 'Cities'
        
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    county = models.ForeignKey(County, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50, default='Nairobi')

    def __str__(self):
        return "Country: {}, County: {}, Name: {}".format(
            self.country.name,
            self.county.name,
            self.name
        )
