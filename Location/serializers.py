from rest_framework.serializers import ModelSerializer

#---------------------------------------------------------#
from .models import City, County, Country

class CountrySerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'code', 'name')


class CountySerializer(ModelSerializer):

    class Meta:
        model = County
        fields = ('id', 'name')


class CitySerializer(ModelSerializer):

    country = CountrySerializer()
    county = CountySerializer()

    class Meta:
        model = City
        fields = ('id', 'county', 'country', 'name')
