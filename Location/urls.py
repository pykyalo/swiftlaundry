from django.conf.urls import url, include

from .views import CityView

urlpatterns = [
    url(r'^cities', CityView.as_view(), name='cities')
]
