from .models import City
from .serializers import CitySerializer

#---------------------------------------------------------#
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response


class CityView(APIView):
    def get(self, request, format=None):
        cities = City.objects.all()
        cities_serializer = CitySerializer(cities, many=True)

        return Response(cities_serializer.data, status=status.HTTP_200_OK)
