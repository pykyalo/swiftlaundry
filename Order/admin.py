from django.contrib import admin

from .models import (
    Line,
    Order,
    Customer,
    OrderAssignment,
    ShippingAddress
)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('number','outlet', 'status', 'date_received')
    search_fields = ['title', 'number']
admin.site.register(Order, OrderAdmin)


class LineAdmin(admin.ModelAdmin):
    list_display = ('order', 'item', 'quantity')
    search_fields = ['order', 'item']
admin.site.register(Line, LineAdmin)


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('user', 'phonenumber', 'email', 'added_by', 'is_banned')
    search_fields = ['user', 'phonenumber', 'email']
admin.site.register(Customer, CustomerAdmin)


class OrderAssignmentAdmin(admin.ModelAdmin):
    list_display = ('order', 'assigned_to', 'assigned_by', 'assigned_on')
    search_fields = ['order']
admin.site.register(OrderAssignment, OrderAssignmentAdmin)



class ShippingAddressAdmin(admin.ModelAdmin):
    list_display = ('customer', 'order')
    search = ['customer']
admin.site.register(ShippingAddress, ShippingAddressAdmin)
