# Generated by Django 2.0.7 on 2018-08-09 06:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Order', '0003_auto_20180809_0611'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='email',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
