# Generated by Django 2.0.7 on 2019-02-04 20:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Order', '0017_auto_20190204_1952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='outlet',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='Outlet.Outlet'),
        ),
    ]
