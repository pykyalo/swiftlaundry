from django.db import models
from django.contrib.auth.models import User
from Auth.models import Profile
from Stockrecord.models import Item
from Outlet.models import Outlet


class Customer(models.Model):
    """
        In Swift Clean, A Customer is a person who takes his item to a
        laundry outlet for cleaning
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    phone_local = models.CharField(max_length=4, default='254')
    phonenumber = models.CharField(max_length=15, unique=True)
    email = models.CharField(max_length=30, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(Profile, on_delete=models.CASCADE)
    is_banned = models.BooleanField(default=False)

    def __str__(self):
        return "{}, {}".format(self.user.first_name, self.phonenumber)


class Order(models.Model):

    ORDER_STATUSES = (
        ('NEW', 'New'),
        ('CONFIRMED', 'Confirmed'),
        ('ASSIGNED', 'Assigned'),
        ('CANCELLED', 'Cancelled'),
        ('REFUNDED', 'Refunded'),
        ('PROCESSING', 'Processing'),
        ('COMPLETED', 'Completed'),
        ('COLLECTED', 'Collected'),
        ('PAYMENT INCOMPLETE', 'Payment Incomplete'),
        ('PAYMENT COMPLETE', 'Payment Complete')
    )
    outlet = models.ForeignKey(
        Outlet, related_name='orders', on_delete=models.CASCADE, null=True
    )
    number = models.CharField(max_length=20, unique=True)
    status = models.CharField(max_length=20, choices=ORDER_STATUSES)
    date_received = models.DateTimeField(auto_now_add=True)
    date_to_collect = models.DateTimeField()

    @property
    def customer_first_name(self):
        return self.shipping_address.customer.user.first_name

    @property
    def order_cost(self):
        lines = self.lines.all()
        total_cost = 0

        for line in lines:
            cost_per_line = int(line.item.price) * int(line.quantity)
            total_cost += cost_per_line
        return total_cost

    def __str__(self):
        return self.number


class Line(models.Model):
    order = models.ForeignKey(
        Order, related_name='lines', on_delete=models.CASCADE
    )
    quantity = models.IntegerField(default=0)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    description = models.TextField()
    caution_instruction = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {}".format(self.order.number, self.item.title)


class ShippingAddress(models.Model):
    order = models.OneToOneField(
        Order, related_name='shipping_address', on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    def __str__(self):
        return self.customer.phonenumber


class OrderAssignment(models.Model):
    """
        Orders are assigned to workers by the person in charge
    """

    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    assigned_to = models.ForeignKey(Profile, on_delete=models.CASCADE)
    assigned_by = models.ForeignKey(User, on_delete=models.CASCADE)
    instruction = models.CharField(max_length=250, null=True)
    assigned_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.order.number
