#---------------------model imports--------------------#
from .models import (
    Customer,
    Order,
    Line,
    ShippingAddress,
    OrderAssignment
)


#----------------------serializer imports---------------#
from Auth.serializers import (
    UserSerializer,
    ProfileSerializer
)
from Stockrecord.serializers import (
    CategorySerializer,
    ItemSerializer
)

from Outlet.serializers import OutletSerializer

#---------------rest_framework imports------------------#
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers


class LeanCustomerSerializer(ModelSerializer):
    class Meta:
        model = Customer

        fields = (
            'user',
            'phone_local',
            'phonenumber',
            'email',
            'date_created',
            'added_by',
            'is_banned'
        )


class CustomerSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Customer
        fields = (
            'id',
            'user',
            'phone_local',
            'phonenumber',
            'email',
            'date_created',
            'added_by',
            'is_banned'
        )


class OrderSerializer(ModelSerializer):
    payment_status = serializers.SerializerMethodField()
    phonenumber = serializers.SerializerMethodField()

    def get_phonenumber(self, order):
        phonenumber = order.shipping_address.customer.phonenumber
        return phonenumber

    def get_payment_status(self, order):
        payment = order.payments.all().first()

        if payment:
            return payment.status
        return False

    class Meta:
        model = Order
        fields = (
            'number',
            'outlet',
            'status',
            'date_received',
            'date_to_collect',
            'order_cost',
            'payment_status',
            'phonenumber'
        )


class LeanOrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'number',
            'outlet',
            'status',
            'date_received',
            'date_to_collect'
        )


class LeanLineSerializer(ModelSerializer):
    item = serializers.SerializerMethodField()
    cost = serializers.SerializerMethodField()

    def get_item(self, line):
        item_name = line.item.title
        return item_name
    
    def get_cost(self, line):
        cost = line.item.price * line.quantity
        return cost

    class Meta:
        model = Line
        fields = (
            'order',
            'quantity',
            'item',
            'cost',
            'description',
            'caution_instruction',
            'date_created'
        )


class LineSerializer(ModelSerializer):
    
    class Meta:
        model = Line
        fields = (
            'order',
            'quantity',
            'item',
            'description',
            'caution_instruction',
            'date_created'
        )


class LeanShippingAddressSerializer(ModelSerializer):
    class Meta:
        model = ShippingAddress
        fields = (
            'order',
            'customer'
        )


class ShippingAddressSerializer(ModelSerializer):
    order = OrderSerializer()
    customer = CustomerSerializer()

    class Meta:
        model = ShippingAddress
        fields = (
            'order',
            'customer'
        )


class LeanOrderAssignmentSerializer(ModelSerializer):
    class Meta:
        model = OrderAssignment
        fields = (
            'order',
            'assigned_to',
            'assigned_by',
            'instruction',
            'assigned_on'
        )


class OrderAssignmentSerializer(ModelSerializer):
    order = OrderSerializer()
    assigned_to = ProfileSerializer()
    assigned_by = UserSerializer()

    class Meta:
        model = OrderAssignment
        fields = (
            'order',
            'assigned_to',
            'assigned_by',
            'instruction',
            'assigned_on'
        )
