from django.conf.urls import url, include

from .views import (
    GetCustomerView,
    CustomerView,
    OrderView,
    OrderAssignmentView,
    OrdersSummaryView,
    OrderDetailView
)


urlpatterns = [
    url(r'^customer/$', CustomerView.as_view(), name='customer'),
    url(r'^getcustomer/(?P<phonenumber>-?\w+)/$', GetCustomerView.as_view(), name='getcustomer'),
    url(r'^order/$', OrderView.as_view(), name='order'),
    url(r'^orders/(?P<number>-?\w+)/$', OrderDetailView.as_view(), name='order-details'),
    url(r'^orders/(?P<outlet_id>-?\w+)/orders-summary/$', OrdersSummaryView.as_view(), name='orders-summary'),
    url(r'^orderassignment/$', OrderAssignmentView.as_view(), name='order-assignment'),

]
