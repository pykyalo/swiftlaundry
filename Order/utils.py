import random

def create_order_number():
    first_three = random.randint(100, 999)
    last_three = random.randint(100, 999)
    return str(first_three) + str(last_three)
    

"""
    Ocassionally we'll need unique data. This functions returns unique
    list of dictionaries
"""

def create_unique_list_of_dictionaries(key, list_of_dictionaries):
    """
        key | An attribute of the dictionary common in all
            | Can be 'id', 'number', 'username'
            | Must NOT be a key whose value is hashable i.e mutable
    """
    unique_list = list({v[key]:v for v in list_of_dictionaries}.values())

    return unique_list
