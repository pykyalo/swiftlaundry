#-------------------python3 modules-------------------#
from datetime import datetime

# -------------------django imports-------------------#
from django.contrib.auth.models import User

#-------------------rest imports----------------------#
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

#-----------------serializer imports-------------------#
from .serializers import (
    CustomerSerializer,
    LeanCustomerSerializer,
    LeanOrderSerializer,
    LeanLineSerializer,
    OrderSerializer,
    LineSerializer,
    LeanShippingAddressSerializer,
    LeanOrderAssignmentSerializer,
    OrderAssignment
)


#-----------------models imports-----------------------#
from .models import Customer, ShippingAddress, Order
from Payment.models import PartnerPaymentTransaction
from Auth.models import Profile
from Outlet.models import (
    OutletStaff,
    Outlet
)

#---------------------import utils---------------------#
from Communication.utils import create_message
from .utils import (
    create_unique_list_of_dictionaries,
    create_order_number
)


class OrdersSummaryView(APIView):
    """
        Returns a summary of an outlet's orders information

    """

    def get(self, request, outlet_id, format=None):
        outlet = Outlet.objects.get(id=outlet_id)
        orders = outlet.orders.all()
        all_orders = orders.count()
        completed_orders = orders.filter(status='PAYMENT COMPLETE').count()
        cancelled_orders = orders.filter(status='CANCELLED').count()
        new_orders = orders.filter(status='NEW').count()
        payments = PartnerPaymentTransaction.objects.filter(order__in=orders)

        current_month = datetime.now().month
        payments_this_month = payments.filter(
            date_created__month=current_month)

        total_amount = 0
        total_this_month = 0

        for transaction in payments_this_month:
            total_this_month += transaction.amount

        for transaction in payments:
            total_amount += transaction.amount

        content = {}
        content['all_orders'] = all_orders
        content['completed_orders'] = completed_orders
        content['cancelled_orders'] = cancelled_orders
        content['new_orders'] = new_orders
        content['total_earnings'] = round(total_amount)
        content['total_earnings_current_month'] = round(total_this_month)

        return Response(content, status=status.HTTP_200_OK)


class OrderAssignmentView(APIView):
    """
        POST | returns staff order assigment details
             | Must be authenicated
        ATTRs | number - the order number
              | assigned_to - staff_id
              | assigned_by - current user
              | instruction - instruction attached to this assignment
        POST BODY {
            "order_id": <order_id>,
            "assigned_to_id": <profile_id>,
            "instruction": "Do NOT add perfume after cleaning"
        }
    """

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        """A shop worker can assign order(s) to fellow worker"""

        user = request.user

        assigned_by = user.id
        assigned_to = request.data['assigned_to_id']
        instruction = request.data['instruction']
        order = request.data['order_id']

        order_assigment_qs = OrderAssignment.objects.filter(order_id=order)

        if not order_assigment_qs.exists():

            data = {
                "order": order,
                "assigned_to": assigned_to,
                "assigned_by": assigned_by,
                "instruction": instruction
            }

            order_assigment_serializer = LeanOrderAssignmentSerializer(
                data=data
            )

            if order_assigment_serializer.is_valid():
                new_assignment = order_assigment_serializer.save()

                # Alert user that assignment has been done
                body = 'Hello {}, you have been assigned #{} by {}. Order status is {} and due on {}'.format(
                    new_assignment.assigned_to.user.first_name,
                    new_assignment.order.number,
                    user.first_name,
                    new_assignment.order.status,
                    new_assignment.order.date_to_collect
                )
                sent_to = '254' + '724230064'
                create_message(sent_to, 'GENERAL', body)

                return Response(
                    order_assigment_serializer.data,
                    status=status.HTTP_201_CREATED
                )
            return Response(
                order_assigment_serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        else:
            # If assignment already done, just update it
            order_assignment_obj = order_assigment_qs.first()
            order_obj = Order.objects.get(id=order)
            assigned_to_obj = Profile.objects.get(id=assigned_to)

            order_assignment_obj.order = order_obj
            order_assignment_obj.assigned_by = user
            order_assignment_obj.assigned_to = assigned_to_obj
            order_assignment_obj.instruction = instruction
            order_assignment_obj.save()

            # Alert user that assignment has been done
            body = 'Hello {}, you have been assigned #{} by {}. Order status is {} and due on {}'.format(
                order_assignment_obj.assigned_to.user.first_name,
                order_assignment_obj.order.number,
                user.first_name,
                order_assignment_obj.order.status,
                order_assignment_obj.order.date_to_collect
            )
            sent_to = '254' + '724230064'
            create_message(sent_to, 'GENERAL', body)

            order_assigment_serializer = LeanOrderAssignmentSerializer(
                order_assignment_obj
            )
            return Response(
                order_assigment_serializer.data,
                status=status.HTTP_201_CREATED
            )


class OrderDetailView(APIView):
    """
        GET | return the details of an order including lines
            | takes <number> as a url parameter

    """

    def get(self, request, number, format=None):

        orders_queryset = Order.objects.filter(
            number=number
        )

        if orders_queryset.exists():
            order = orders_queryset.first()
            lines = order.lines.all()
            lines_serializer = LeanLineSerializer(lines, many=True)

            order_data = {
                'id': order.id,
                'cost': order.order_cost,
                'number': order.number,
                'phonenumber': order.shipping_address.customer.phonenumber,
                'last_name': order.shipping_address.customer.user.last_name,
                'first_name': order.shipping_address.customer.user.first_name,
                'email': order.shipping_address.customer.user.email,
                'customer_is_banned': order.shipping_address.customer.is_banned,
                'customer_added_by': order.shipping_address.customer.added_by.outlet.name,
                'status': order.status,
                'date_received': order.date_received,
                'date_to_collect': order.date_to_collect,
                'lines': lines_serializer.data
            }
            return Response(order_data, status=status.HTTP_200_OK)

        else:
            error = {
                "error": "Order #{} does not exist.".format(number)
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)


class OrderView(APIView):
    """
        POST | returns created order
             | Must be authenicated
        ATTRs | number - A random string used to uniquely identify an order
              | status - A value updated in the different stages of na order
              | date_to_collect -When can a customer come for the order\n
        POST BODY {
            "outlet_id": 1,
            "status": "NEW",
            "date_to_collect": "2018-08-25T19:48:38+03:00",
            "lines": [
                {
                    "item_id": 1,
                    "description": "Please handle with care",
                    "caution_instruction": "No detergents".
                    "quantity": 12
                },
                {
                    "item_id": 2,
                    "description": "Please handle with care",
                    "caution_instruction": "No detergents",
                    "quantity" 10
                },
            ]
        }

    """

    permission_classes = (IsAuthenticated, )

    def put(self, request, number, format=None):
        user = request.user

        profiles = Profile.objects.filter(user_id=user.id)

        try:
            profile_obj = profiles.first()
        except:
            # No Profile Found
            error = {
                'error': 'Request from invalid user'
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        user_outlet = OutletStaff.objects.filter(
            staff_id=profile_obj.id).first()

        orders_queryset = Order.objects.filter(
            number=number,
            outlet_id=user_outlet.outlet.id
        )

        if orders_queryset.exists():

            order_data = {
                "number": number,
                "outlet": user_outlet.outlet.id,
                'status': request.data['status'],
                'date_to_collect': request.data['date_to_collect']
            }

            order_serializer = LeanOrderSerializer(
                orders_queryset.first(), data=order_data, partial=True)

            if order_serializer.is_valid():
                order = order_serializer.save()
                return self.update_lines(order, request.data['lines'])
            else:
                return Response(order_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            error = {
                "error": "Order #{} does not exist in your shop.".format(number)
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

    def update_lines(self, order, lines_data):

        order_lines = order.lines.all()

        for line_data in lines_data:
            line_id = line_data['id']
            item_id = line_data['item_id']

            quantity = line_data['quantity']
            description = line_data['description']
            caution_instruction = line_data['caution_instruction']

            for line in order_lines:

                if line.id == line_id:

                    line_data = {
                        "order": order.id,
                        "item": item_id,
                        "quantity": quantity,
                        "description": description,
                        'caution_instruction': caution_instruction
                    }

                    line_serializer = LeanLineSerializer(
                        line, data=line_data, partial=True)

                    if line_serializer.is_valid():
                        line_serializer.save()
                    else:
                        return Response(line_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        order_serializer = OrderSerializer(order)

        return Response(order_serializer.data, status=status.HTTP_201_CREATED)

    def post(self, request, format=None):
        user = request.user
        outlet_id = request.data['outlet_id']
        number = create_order_number()
        order_status = request.data['status']
        date_to_collect = request.data["date_to_collect"]
        lines = request.data["lines"]
        customer_id = request.data['customer_id']
        
        # check of customer is valid
        try:
            customer = Customer.objects.get(id=customer_id)
        except:
            error = {
                'error': 'Invalid customer in request'
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        # check if outlet id is valid
        try:
            outlet = Outlet.objects.get(id=outlet_id)
        except:
            error = {
                "error": "Invalid outlet in request"
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        order_data = {
            "number": number,
            "outlet": outlet_id,
            "status": order_status,
            "date_to_collect": date_to_collect
        }

        order_serializer = LeanOrderSerializer(data=order_data)

        if order_serializer.is_valid():
            order = order_serializer.save()

            # create shipping address
            shipping_address = ShippingAddress.objects.create(
                order=order,
                customer=customer
            )

            return self.create_order_lines(order, lines, order_serializer)
        else:
            return Response(order_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create_order_lines(self, order, lines, order_serializer):

        for line in lines:
            line_data = {
                "order": order.id,
                "item": line['item_id'],
                "quantity": line["quantity"],
                "description": line["description"],
                "caution_instruction": line["caution_instruction"]
            }

            line_serializer = LineSerializer(data=line_data)

            if line_serializer.is_valid():
                line_serializer.save()
            else:
                return Response(line_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(order_serializer.data, status=status.HTTP_201_CREATED)


class CustomerView(APIView):
    """
        Description \n
            GET | returns all customers added by shop \n

            POST | return created customer
            ACCEPT:  \n
                ```{
                "first_name": "Otieno",
                "last_name": "Oluoch",
                "phonenumber": "0712345678",
                "email": "otis@domain.com",
                }```
    """

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        user = request.user

        profiles = Profile.objects.filter(user_id=user.id)

        try:
            profile_obj = profiles.first()
        except:
            # No Profile Found
            error = {
                'error': 'Request from invalid user'
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        user_outlets = OutletStaff.objects.filter(staff_id=profile_obj.id)

        try:
            user_outlet_obj = user_outlets.first()
        except:
            # No Outlet Found
            error = {
                'error': 'User laundry services does not exist'
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        # Now Get the Orders for the Outlet
        outlet_orders = Order.objects.filter(
            outlet_id=user_outlet_obj.outlet.id)

        return self.get_customers(outlet_orders, user_outlet_obj)

    def get_customers(self, outlet_orders, user_outlet_obj):
        if outlet_orders.exists():
            customers = []

            # Get all Customers
            for order in outlet_orders:
                shipping_address = ShippingAddress.objects.filter(
                    order_id=order.id
                )

                if shipping_address.exists():
                    customer = shipping_address.first().customer
                    customer_serializer = CustomerSerializer(customer)
                    customers.append(customer_serializer.data)

            customers = create_unique_list_of_dictionaries(
                'phonenumber', customers)

            return Response(customers, status=status.HTTP_200_OK)
        else:
            error = {
                'error': '{} has not interacted with customers so far'.format(
                    user_outlet_obj.outlet.name
                )
            }

            # Send them a message to remind shop shop owner to use swift clean
            # should be a background task
            body = 'Hello {}, your shop {} on Swift Clean has not\
                    interacted with customers yet. We encourage you to use SWIFT CLEAN when receiving orders in your shop to enjoy our services. Thank you.'.format(
                user_outlet_obj.outlet.admin.user.first_name,
                user_outlet_obj.outlet.name
            )
            sent_to = '254' + user_outlet_obj.outlet.admin.phonenumber
            create_message(sent_to, 'GENERAL', body)

            return Response(error, status=status.HTTP_404_NOT_FOUND)

    def post(self, request, format=None):
        user = request.user
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        email = request.data['email']
        phonenumber = request.data['phonenumber']

        customer_exists = Customer.objects.filter(phonenumber=phonenumber)

        if customer_exists.exists():
            customer_serializer = CustomerSerializer(customer_exists.first())
            return Response(customer_serializer.data, status=status.HTTP_200_OK)
        else:
            profile = Profile.objects.filter(user_id=user.id)

            if not profile.exists():
                error = {
                    'error': 'Request By Invalid User'
                }
                return Response(error, status=status.HTTP_400_BAD_REQUEST)

            customer_user_object = self.create_user(
                first_name, last_name, email, phonenumber
            )

            customer_data = {
                'user': customer_user_object,
                'phone_local': '254',
                'phonenumber': phonenumber,
                'email': email,
                'added_by': profile.first().id,
                'is_banned': False
            }

            return self.create_customer(customer_data)

    def create_customer(self, data):
        customer_serializer = LeanCustomerSerializer(data=data)

        if customer_serializer.is_valid():
            new_customer = customer_serializer.save()

            new_customer_serializer = CustomerSerializer(new_customer)

            return Response(
                new_customer_serializer.data, status=status.HTTP_200_OK
            )
        return Response(
            customer_serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )

    def create_user(self, first_name, last_name, email, phonenumber):
        new_user = User.objects.create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            username=phonenumber
        )
        new_user.save()

        user = User.objects.get(id=new_user.id)

        return user.id


class GetCustomerView(APIView):
    """
        Description \n
            GET | return a customer object : param | phonenumber
    """

    def get(self, request, phonenumber, format=None):
        customer_qs = Customer.objects.filter(phonenumber=phonenumber)

        if customer_qs.exists():
            customer = customer_qs.first()
            customer_serializer = CustomerSerializer(customer)
            return Response(customer_serializer.data, status=status.HTTP_200_OK)
        else:
            error = {
                'error': 'Customer Not Found'
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)
