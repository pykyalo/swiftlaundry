from django.contrib import admin

from Outlet.models import Outlet, OutletStaff


class OutletAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'phonenumber',
        'mpesa_number',
        'city',
        'location_text',
        'date_created'
    )

    search_fields = ['name', 'id', 'location_text']

admin.site.register(Outlet, OutletAdmin)


class OutletStaffAdmin(admin.ModelAdmin):
    list_display = (
        'staff',
        'outlet',
        'date_created'
    )

    search_fields = ['outlet', 'staff']

admin.site.register(OutletStaff, OutletStaffAdmin)
