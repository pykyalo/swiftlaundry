from django.db import models

#***********************************************************#
from Auth.models import Profile
from Location.models import City


class Outlet(models.Model):
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=150, null=True)
    admin = models.ForeignKey(
        Profile, null=True, on_delete=models.CASCADE, related_name='outlet')
    phonenumber = models.CharField(max_length=15, null=True)
    mpesa_number = models.CharField(max_length=15, null=True)
    date_created = models.DateTimeField(auto_now=True)
    last_updated = models.DateTimeField(auto_now_add=True)
    website = models.CharField(max_length=250, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    location_text = models.TextField(null=True)
    longitude = models.FloatField(null=True)
    latitude = models.FloatField(null=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class OutletStaff(models.Model):
    outlet = models.ForeignKey(
        Outlet, related_name='outletstaff', on_delete=models.CASCADE)
    staff = models.ForeignKey(Profile, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        context = "{} {}".format(self.staff.user.first_name, self.outlet.name)
        return context
