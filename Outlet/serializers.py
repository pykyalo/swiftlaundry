from rest_framework.serializers import ModelSerializer

#--------------------------------------------------------------#
from .models import Outlet, OutletStaff
from Auth.models import Profile
from Location.models import City

#--------------------------------------------------------------#
from Location.serializers import CitySerializer
from Auth.serializers import ProfileSerializer


class LeanOutletSerializer(ModelSerializer):
    class Meta:
        model = Outlet
        fields = (
            'name',
            'slug',
            'admin',
            'website',
            'phonenumber',
            'mpesa_number',
            'city',
            'location_text',
            'longitude',
            'latitude',
            'description'
        )


class OutletSerializer(ModelSerializer):

    city = CitySerializer()
    admin = ProfileSerializer()

    class Meta:
        model = Outlet
        fields = (
            'id',
            'name',
            'slug',
            'admin',
            'website',
            'phonenumber',
            'mpesa_number',
            'city',
            'location_text',
            'longitude',
            'latitude',
            'description'
        )


class OutletStaffSerializer(ModelSerializer):
    outlet = OutletSerializer()
    staff = ProfileSerializer()

    class Meta:
        model = OutletStaff
        fields = ('id', 'outlet', 'staff')


class LeanOutletStaffSerializer(ModelSerializer):
    class Meta:
        model = OutletStaff
        fields = ('outlet', 'staff')
