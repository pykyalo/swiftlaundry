from django.conf.urls import include, url

from .views import OutletDetailsView, OutletView, OutletOrdersView, OutletPaymentsView

urlpatterns = [
    url(r'^outlet/$', OutletView.as_view(), name='outlet'),
    url(r'^outlets/(?P<pk>\d+)/$', OutletDetailsView.as_view(), name='outlet-details'),
    url(r'^outlets/(?P<pk>\d+)/orders/$', OutletOrdersView.as_view(), name='outlet-orders'),
    url(r'^outlets/(?P<pk>\d+)/payments/$', OutletPaymentsView.as_view(), name='outlet-payments')
]
