from slugify import slugify

#--------------------------------------------------------#
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response

#-------------------------------------------------------#
from .models import Outlet, OutletStaff
from Auth.models import Profile
from .serializers import OutletSerializer, LeanOutletSerializer
from Order.serializers import OrderSerializer
from Payment.models import PartnerPaymentTransaction
from Payment.serializers import PartnerPaymentTransactionSerializer


class OutletPaymentsView(APIView):
    """
        GET | returns payments that have been done to this shop
    """

    def get(self, request, pk):
        outlet = Outlet.objects.get(id=pk)
        orders = outlet.orders.filter(status='PAYMENT COMPLETE')
        partner_payments = PartnerPaymentTransaction.objects.filter(order__in=orders)
        serializer = PartnerPaymentTransactionSerializer(partner_payments, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class OutletOrdersView(APIView):
    """
        GET | returns orders that belong to a shop
            | takes <pk> as a url parameter
    """

    def get(self, request, pk):
        outlet = Outlet.objects.get(id=pk)

        serializer = OrderSerializer(outlet.orders.all().order_by('-date_received'), many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class OutletDetailsView(APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = OutletSerializer
    lean_serializer_class = LeanOutletSerializer

    def delete(self, request, pk, format=None):
        outlet = Outlet.objects.get(id=pk)
        outlet_staff_qs = OutletStaff.objects.filter(outlet_id=outlet.id)
        outlet_staff_qs.delete()
        outlet.delete()

        return Response(status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        user = request.user
        profile = Profile.objects.get(user_id=user.id)
        name = request.data['name']
        outlet = Outlet.objects.get(id=pk)

        if profile.is_outlet_admin == False:
            error = {
                'error': 'Sorry {}, You do not have Swift Verified Rights to update a Shop.'.format(user.first_name)
            }

            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        slug = self.create_slug(name)
        admin = profile.id
        website = request.data['website']
        location_text = request.data['location_text']
        description = request.data['description']
        city = request.data['city']
        longitude = request.data['longitude']
        latitude = request.data['latitude']
        phonenumber = request.data["phonenumber"]
        mpesa_number = request.data["mpesa_number"]

        data = {
            'name': name,
            'slug': slug,
            'admin': admin,
            'website': website,
            'city': city,
            'location_text': location_text,
            'description': description,
            'longitude': longitude,
            'latitude': latitude,
            'phonenumber': phonenumber,
            'mpesa_number': mpesa_number
        }

        obj_serializer = self.lean_serializer_class(
            outlet, data=data, partial=True)

        if obj_serializer.is_valid():
            outlet = obj_serializer.save()
            content = self.serializer_class(outlet)

            return Response(content.data, status=status.HTTP_200_OK)
        return Response(obj_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create_slug(self, name):
        return slugify(name)


class OutletView(APIView):
    """
        Description: A User with is_outlet_admin can create an outlet\n
        POST\n
        {
        "name": "Safi Sana Laundry Services",
        "website": "www.safisanalaundry.com",
        "city": 1,
        "location_text": "Bishop Magua Plaza, George Padmore Lane",
        "description": "Room 12, Left Wing"
        "longitude": -3.222234,
        "latitude": 34.3333444,
        "mpesa_number": "0712345678",
        "phonenumber": "07123456789"
        }
    """

    permission_classes = (IsAuthenticated, )
    serializer_class = OutletSerializer
    lean_serializer_class = LeanOutletSerializer

    def get(self, request, format=None):
        user = request.user

        profile = Profile.objects.get(user_id=user.id)

        outlet_staff_qs = OutletStaff.objects.filter(staff_id=profile.id)

        if outlet_staff_qs:
            outlet_staff_obj = outlet_staff_qs.first()
            outlet = Outlet.objects.get(id=outlet_staff_obj.outlet.id)
            outlet_serializer = self.serializer_class(outlet)
            return Response(outlet_serializer.data, status=status.HTTP_200_OK)

        error = {
            'error': 'Sorry, your account has been linked to any shop.'
        }

        return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        user = request.user

        profile = Profile.objects.get(user_id=user.id)
        name = request.data['name']
        outlet = Outlet.objects.filter(admin_id=profile.id)

        if outlet.exists():
            error = {
                'error': 'You already have a shop - {}'.format(outlet.first().name)
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if profile.is_outlet_admin == False:
            error = {
                'error': 'Sorry {}, You do not have Swift Verified Rights to Create a Shop.'.format(user.first_name)
            }

            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if Outlet.objects.filter(name=name).exists():
            error = {
                'error': 'Sorry, {} already exists.'.format(name)
            }

            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        slug = self.create_slug(name)
        admin = profile.id
        website = request.data['website']
        location_text = request.data['location_text']
        description = request.data['description']
        city = request.data['city']
        longitude = request.data['longitude']
        latitude = request.data['latitude']
        phonenumber = request.data["phonenumber"]
        mpesa_number = request.data["mpesa_number"]

        data = {
            'name': name,
            'slug': slug,
            'admin': admin,
            'website': website,
            'city': city,
            'location_text': location_text,
            'description': description,
            'longitude': longitude,
            'latitude': latitude,
            'phonenumber': phonenumber,
            'mpesa_number': mpesa_number
        }

        obj_serializer = self.lean_serializer_class(data=data)

        if obj_serializer.is_valid():
            outlet = obj_serializer.save()
            content = self.serializer_class(outlet)

            # add shop admin as staff
            outlet_staff = OutletStaff.objects.create(
                outlet=outlet,
                staff=profile
            )
            outlet_staff.save()

            return Response(content.data, status=status.HTTP_200_OK)
        return Response(obj_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create_slug(self, name):
        return slugify(name)
