from django.contrib import admin

from .models import STKTransaction, PaymentTransaction, PartnerPaymentTransaction, Commission


class CommissionAdmin(admin.ModelAdmin):
    list_display = [
        'value',
        'date_created'
    ]


admin.site.register(Commission, CommissionAdmin)


class PartnerPaymentTransactionAdmin(admin.ModelAdmin):
    list_display = [
        'phone_number',
        'order',
        'amount',
        'transaction_id',
        'date_created'
    ]

    search_fields = ('order__number', 'transaction_id')


admin.site.register(PartnerPaymentTransaction, PartnerPaymentTransactionAdmin)


class STKTransactionAdmin(admin.ModelAdmin):
    list_display = [
        'phone_number',
        'amount',
        'account_number',
        'order',
        'date_created'
    ]
    search_fields = ('order__number', 'account_number', )


admin.site.register(STKTransaction, STKTransactionAdmin)


class PaymentTransactionAdmin(admin.ModelAdmin):
    list_display = [
        'phone_number',
        'amount',
        'account_number',
        'transaction_id',
        'order',
        'transaction_type',
        'date_created'
    ]
    search_fields = ('order__number', 'account_number', 'transaction_id')


admin.site.register(PaymentTransaction, PaymentTransactionAdmin)
