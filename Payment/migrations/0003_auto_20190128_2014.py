# Generated by Django 2.0.7 on 2019-01-28 20:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Payment', '0002_paymenttransaction_date_created'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stktransaction',
            name='phone_number',
            field=models.CharField(max_length=25),
        ),
    ]
