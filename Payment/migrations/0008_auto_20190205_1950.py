# Generated by Django 2.0.7 on 2019-02-05 19:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Payment', '0007_partnerpaymenttransaction'),
    ]

    operations = [
        migrations.CreateModel(
            name='Commission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.IntegerField(default=20)),
                ('date_created', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='partnerpaymenttransaction',
            name='commission_value',
            field=models.IntegerField(default=0),
        ),
    ]
