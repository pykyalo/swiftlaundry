from email.policy import default

from django.db import models
from Order.models import Order


class Commission(models.Model):
    """
        Desc:
            - SC wants to have an adjustable commission on orders
            - This model allows for a dynamic setting of the commission value
    """
    value = models.IntegerField(default=20)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Current commision value is {}".format(self.value)


class PartnerPaymentTransaction(models.Model):
    """
        Desc:
            - This model is used to create a payment to a partner.
            - This action is triggered on a complete payment
            - Has the following fields:
                1. order - which order is being paid for
                2. transaction_id - the MPESA code for the transaction after a successfull transaction
                3. transaction_date - the MPESA date 
                4. date_created - server time for the transaction when initialized
                5. amount - amount being paid. commuted based on the commission set
                6. phone_number - the receiver of the money
    """

    # view to show partner payments
    # total earnings

    TRANSACTION_STATUSES = (
        ('PENDING', 'Pending'),
        ("FAILED", "Failed"),
        ("COMPLETE", "Complete")
    )

    order = models.ForeignKey(
        Order, related_name='partner_payments', on_delete=models.CASCADE)
    transaction_id = models.CharField(max_length=30, null=True)
    transaction_date = models.CharField(max_length=30, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField()
    commission_value = models.IntegerField(default=0)
    phone_number = models.CharField(max_length=20)
    message = models.CharField(max_length=256, null=True, blank=True)
    transaction_status = models.CharField(
        max_length=100, choices=TRANSACTION_STATUSES, default=TRANSACTION_STATUSES[0])

    def get_partner(self):
        return self.order.outlet.admin  # return profile

    def __str__(self):
        return "{} - {} - {}".format(self.phone_number, self.order.number, self.amount)


class STKTransaction(models.Model):
    """
    Desc:
        - Swift clean uses third party APIs to handle payments
        - This model is used for all stk push transactions
        - An stk transaction has:
                1. phone_number - customer's number 
                2. amount - amount being paid
                3. account_number - a unique number used to identify the current transaction's account
                4. order - the order being paid for
                5. date_created
                6. status - was the stk request receieved?
    """
    phone_number = models.CharField(max_length=25)
    amount = models.FloatField()
    account_number = models.CharField(max_length=10)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {} - {}".format(self.account_number, self.order.number, self.phone_number)


class PaymentTransaction(models.Model):
    """
    Desc:
        - This model is used for all transactions
        - A payment transaction has:
                1. transaction_type - is it an stk push or  used paybill?
                2. amount - amount being paid
                3. phone_number - customer's number
                4. account_number - a unique number used to identify the current transaction's account
                5. order - the order being paid for 
                6. transaction_id - mpesa's transaction id
                7. transaction_date - mpesa's transaction_date
                8. status - is the transaction a success?

    """

    STKPUSH, PAYBILL = 'stkpush', 'paybill'

    TRANSACTION_TYPES = (
        (STKPUSH, 'STK Push'),
        (PAYBILL, 'Pay Bill')
    )

    transaction_type = models.CharField(
        max_length=50,
        choices=TRANSACTION_TYPES,
        default=TRANSACTION_TYPES[0]
    )

    amount = models.FloatField()
    paid_amount = models.FloatField(null=True)
    phone_number = models.CharField(max_length=25)
    account_number = models.CharField(max_length=10)
    order = models.ForeignKey(
        Order, related_name='payments', on_delete=models.CASCADE)

    # fields updated on confirmation
    transaction_id = models.CharField(max_length=10, null=True)
    transaction_date = models.CharField(max_length=50, null=True)
    status = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {} - {}".format(self.account_number, self.order.number, self.phone_number)
