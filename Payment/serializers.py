from rest_framework import serializers

from .models import PaymentTransaction, STKTransaction, PartnerPaymentTransaction
from Order.models import Order


class PartnerPaymentTransactionSerializer(serializers.ModelSerializer):

    order_number = serializers.SerializerMethodField()

    def get_order_number(self, partnerpaymenttransaction):
        order = Order.objects.get(id=partnerpaymenttransaction.order.id)
        return order.number

    class Meta:
        model = PartnerPaymentTransaction
        fields = (
            'order_number',
            'transaction_id',
            'transaction_date',
            'date_created',
            'amount',
            'commission_value',
            'phone_number',
            'message',
            'transaction_status'
        )


class STKTransactionSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        stk_transaction = STKTransaction.objects.create(**validated_data)

        return stk_transaction

    class Meta:
        model = STKTransaction
        fields = ('__all__')


class PaymentTransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentTransaction
        fields = ('__all__')
