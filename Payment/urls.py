from django.conf.urls import url, include

from .views import (
    STKTransactionView,
    PaymentTransactionView
)


urlpatterns = [
    url(r'^stk-transaction/$', STKTransactionView.as_view(), name='stk-transaction'),
    url(r'^payment-transaction/$', PaymentTransactionView.as_view(), name='payment-transaction'),
]
