import random
import requests
from rest_framework import status
from rest_framework.response import Response
from Communication.utils import create_message

from Payment.models import PartnerPaymentTransaction, Commission


def get_account_number():
    first_3 = random.randint(111, 999)
    last_3 = random.randint(111, 999)
    account_number = "SC{}{}".format(first_3, last_3)  # SC is Swift Clean
    return account_number


def generate_account_number(STKTransaction):
    # we keep it simple for now

    account_number = get_account_number()

    stk_transaction = STKTransaction.objects.filter(
        account_number=account_number)

    if stk_transaction.exists():
        account_number = get_account_number()
        return account_number
    return account_number


def handle_payment_to_merchant(order):
    amount_before_commission = order.order_cost
    commission = Commission.objects.all().first()

    amount_after_commission = amount_before_commission - \
        (int(amount_before_commission) * int(commission.value) / 100)

    phone_number = order.outlet.admin.phonenumber
    phone_local = order.outlet.admin.phone_local
    merchant_phone = str(phone_local) + str(phone_number[1:])

    partner_transaction = PartnerPaymentTransaction.objects.create(
        order=order,
        amount=amount_after_commission,
        commission_value=float(amount_before_commission) *
        int(commission.value) / 100,
        phone_number=merchant_phone
    )

    b_2_c_url = 'http://23.239.12.46/laravel/swiftpay/public/api/v1/b2c'
    b_2_c_data = {
        "phone": merchant_phone,
        "amount": round(amount_after_commission)
    }
    response = requests.post(b_2_c_url, b_2_c_data)

    is_success = response.json()['status']
    conversation_id = response.json()['ConversationID']
    message = response.json()['message']

    if is_success and conversation_id != "":
        # transaction was successfull. Update model

        partner_transaction.transaction_status = 'COMPLETE'
        partner_transaction.message = message
        partner_transaction.save()

        return partner_transaction

    elif is_success and conversation_id == "":
        # low balance
        partner_transaction.transaction_status = 'FAILED'
        partner_transaction.message = message
        partner_transaction.save()
        return partner_transaction

    elif not is_success:
        partner_transaction.transaction_status = 'FAILED'
        partner_transaction.message = message
        partner_transaction.save()
        return partner_transaction


def handle_complete_payment(payment_transaction, order, transaction_id, transaction_date, paid_amount):
    payment_transaction.transaction_id = transaction_id
    payment_transaction.transaction_date = transaction_date
    payment_transaction.transaction = True
    payment_transaction.paid_amount = paid_amount
    payment_transaction.save()

    order.status = 'PAYMENT COMPLETE'
    order.save()

    # handle partner transaction
    partner_payment = handle_payment_to_merchant(order)
    content = {}

    if partner_payment.transaction_status == 'PENDING':
        content = {
            "success": "{} has been successfully paid.".format(order.number),
            "status": "{}".format(order.status),
            "merchant_payment": {
                "amount": partner_payment.amount,
                "phone_number": payment_transaction.phone_number,
                "status": partner_payment.transaction_status
            }
        }

    elif partner_payment.transaction_status == 'COMPLETE':
        content = {
            "success": "{} has been successfully paid.".format(order.number),
            "status": "{}".format(order.status),
            "merchant_payment": {
                "amount": partner_payment.amount,
                "phone_number": payment_transaction.phone_number,
                "status": partner_payment.transaction_status
            }
        }
    elif partner_payment.transaction_status == 'FAILED':
        content = {
            "success": "{} has been successfully paid.".format(order.number),
            "status": "{}".format(order.status),
            "merchant_payment": {
                "amount": partner_payment.amount,
                "phone_number": payment_transaction.phone_number,
                "status": partner_payment.transaction_status
            }
        }

    message = "Hello {}, order #{} has been fully paid. Thank for being our partner. - {}".format(
        order.outlet.admin.user.first_name,
        order.number,
        order.outlet.name
    )
    create_message(sent_to=payment_transaction.phone_number,
                   category='Payment', body=message)

    return Response(content, status=status.HTTP_200_OK)


def handle_incomplete_payment(payment_transaction, order, transaction_id, transaction_date, paid_amount):
    payment_transaction.transaction_id = transaction_id
    payment_transaction.transaction_date = transaction_date
    payment_transaction.transaction = True
    payment_transaction.paid_amount = paid_amount
    payment_transaction.save()

    order.status = 'PAYMENT INCOMPLETE'
    order.save()

    content = {
        'success': "{} has NOT been fully paid.".format(order.number),
        'status': "{}".format(order.status)
    }

    balance = order.order_cost - int(float(paid_amount))
    message = "Hello {}, order #{} has not been fully paid. Please pay {} to complete the payment".format(
        order.outlet.admin.user.first_name,
        order.number,
        balance
    )

    create_message(sent_to=payment_transaction.phone_number,
                   category='Payment', body=message)

    return Response(content, status=status.HTTP_200_OK)
