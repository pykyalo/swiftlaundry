from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import PaymentTransaction, STKTransaction
from Order.models import Order
from .serializers import PaymentTransactionSerializer, STKTransactionSerializer
from .utils import generate_account_number, handle_complete_payment, handle_incomplete_payment
from Communication.utils import create_message
import requests
import time


class STKTransactionView(APIView):
    def post(self, request):
        """
            Description: 
                - used to create an stk transaction \n
                - and make request to the 3rd party stk push processor\n
                - expects \n 
                {
                    phone_number = 0712345678\n
                    order_number = 892324\n
                }
        """
        # create STKTransaction obj
        # create PaymentTransaction
        # use requests and make a post to the 3rd party
        order_number = request.data['order_number']

        order = Order.objects.get(number=order_number)
        phone_number = request.data['phone_number']
        account_number = generate_account_number(STKTransaction)
        amount = order.order_cost

        data = {
            "phone_number": phone_number,
            "order": order.id,
            "account_number": account_number,
            "amount": amount
        }

        stk_transaction_serializer = STKTransactionSerializer(data=data)

        if stk_transaction_serializer.is_valid():
            stk_transaction_serializer.save()

            pt_data = {
                "phone_number": phone_number,
                "order": order,
                "account_number": account_number,
                "amount": amount,
                "transaction_type": 'stkpush'
            }

            payment_transaction = PaymentTransaction.objects.create(**pt_data)

            confirm_url = "http://23.239.12.46/laravel/swiftpay/public/api/v1/confirm"

            confirmation_data = {
                "account_number": account_number
            }

            stk_push_url = "http://23.239.12.46/laravel/swiftpay/public/api/v1/stkpush"

            stk_data = {
                'account_number': account_number,
                'amount': amount,
                'phone': phone_number
            }

            stk_response = requests.post(stk_push_url, data=stk_data)

            stk_is_success = stk_response.json()['status']

            # check status code and confirm transation or show failure

            if stk_is_success:
                # wait for one minute for user to pay before confirming
                time.sleep(60)

                confirm_payment = requests.post(
                    confirm_url, data=confirmation_data)

                is_success = confirm_payment.json()['status']

                if is_success:
                    paid_amount = confirm_payment.json()['amount']
                    transaction_id = confirm_payment.json()['transaction_id']
                    transaction_date = confirm_payment.json()[
                        'transaction_date']

                    if is_success:

                        if paid_amount >= amount:
                            handle_complete_payment(
                                payment_transaction,
                                order,
                                transaction_id,
                                transaction_date,
                                phone_number,
                                paid_amount
                            )
                        else:
                            handle_incomplete_payment(
                                payment_transaction,
                                order,
                                transaction_id,
                                transaction_date,
                                phone_number,
                                paid_amount
                            )
                    else:
                        confirm_errors = {"error": "Payment does not exist"}
                        return Response(confirm_errors, status=status.HTTP_400_BAD_REQUEST)

                else:
                    confirm_errors = {"error": "Payment does not exist"}
                    return Response(confirm_errors, status=status.HTTP_400_BAD_REQUEST)
            else:

                stk_push_errors = {"error": "STK Transaction failed"}

                return Response(stk_push_errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(stk_transaction_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PaymentTransactionView(APIView):

    def post(self, request, format=None):
        """
            Description: Used to initialize payment and confirm payment
            Step 1: check the payment action - initialize or confirm
            Step 2: if initialize, create payment record
                    if confirm, make a request to 3rd party api
            Step 3 : on success, update payment transactions model,
                     on error, show the error
        """
        action = request.data['action']

        if action == 'initialize_payment':

            order_number = request.data['order_number']
            phone_number = request.data['phone_number']
            order = Order.objects.get(number=order_number)
            amount = order.order_cost

            # generate an account
            account_number = generate_account_number(STKTransaction)

            data = {
                'account_number': account_number,
                'transaction_type': 'paybill',
                'phone_number': phone_number,
                'order': order.id,
                'amount': amount
            }

            # create paymenttransaction model
            payment_transaction_serializer = PaymentTransactionSerializer(
                data=data
            )

            if payment_transaction_serializer.is_valid():
                payment_transaction_serializer.save()

                # Send the customer an sms of 'how to pay'
                # this sms will be very expensive. needs to be shortened
                message = "Hello {},\nPlease pay {},  Ksh. {}  for order number {} using account number {}.\nThe payment details are as follows:\n Paybill number: 904 350 \nAccount number: {} \nAmount: {} \nThank you for being our loyal customer. \n GLASOFT LIMITED".format(
                    order.customer_first_name.title(),
                    order.outlet.name,
                    order.order_cost,
                    order.number,
                    payment_transaction_serializer.data['account_number'],
                    payment_transaction_serializer.data['account_number'],
                    order.order_cost
                )
                create_message(sent_to=phone_number,
                               category='Payment', body=message)

                # info will be used to give payment
                return Response(
                    payment_transaction_serializer.data,
                    status=status.HTTP_200_OK
                )
            return Response(
                payment_transaction_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

        elif action == 'confirm_payment':
            account_number = request.data['account_number']
            confirm_url = "http://23.239.12.46/laravel/swiftpay/public/api/v1/confirm"

            confirmation_data = {
                "account_number": account_number
            }

            payment_transaction = PaymentTransaction.objects.filter(
                account_number=account_number)
            payment_transaction = payment_transaction.first()
            order = payment_transaction.order

            confirm_payment = requests.post(
                confirm_url, data=confirmation_data
            )

            is_success = confirm_payment.json()['status']
            paid_amount = confirm_payment.json()['amount']
            transaction_id = confirm_payment.json()['transaction_id']
            transaction_date = confirm_payment.json()['transaction_date']

            if is_success:

                if int(float(paid_amount)) >= order.order_cost:
                    return handle_complete_payment(
                        payment_transaction,
                        order,
                        transaction_id,
                        transaction_date,
                        paid_amount
                    )
                else:
                    return handle_incomplete_payment(
                        payment_transaction,
                        order,
                        transaction_id,
                        transaction_date,
                        paid_amount
                    )
            else:
                errors = {"error": "Payment transation failed"}
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
