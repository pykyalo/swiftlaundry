from django.contrib import admin

from .models import (
    Category,
    Item
)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_active', 'date_created')
    search_fields = ['title']
admin.site.register(Category, CategoryAdmin)


class ItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_active', 'date_created')
    search_fields = ['category', 'title']

admin.site.register(Item, ItemAdmin)
