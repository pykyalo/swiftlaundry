from django.apps import AppConfig


class StockrecordConfig(AppConfig):
    name = 'Stockrecord'
