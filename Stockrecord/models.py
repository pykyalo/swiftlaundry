from django.db import models


#-------------------models import----------------------#
from Outlet.models import Outlet


class Category(models.Model):
    """
        A category is group of related items
        - Beddings
        - Jeans
        - Robes
        - Curtains
    """
    title = models.CharField(max_length=240, unique=True)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Item(models.Model):
    """
        An Item is a facade checked in for cleaning
            - Cotton shirt
            - Khaki trouser
            - Wedding dress

        It is associtated to a category
    """
    outlet = models.ForeignKey(Outlet, on_delete=models.CASCADE, null=True)
    price = models.FloatField(null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=500)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)

    @property
    def item_name(self):
        # Returns the name of the item with category and title combined
        return "{} - {}".format(self.title, self.category.title)

    def __str__(self):
        return "{} - {}".format(self.title, self.category.title)
