#-----------------------------------------------------#
from .models import (
    Category,
    Item
)

#------------------------------------------------------#
from rest_framework.serializers import ModelSerializer

class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'title',
            'is_active',
            'date_created'
        )


class LeanItemSerializer(ModelSerializer):

    class Meta:
        model = Item
        fields = (
            'category',
            'outlet',
            'price',
            'title',
            'slug',
            'is_active',
            'date_created'
        )

class ItemSerializer(ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Item
        fields = (
	    'id',
            'category',
            'outlet',
            'price',
            'title',
            'slug',
            'is_active',
            'date_created'
        )
