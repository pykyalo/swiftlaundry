from django.conf.urls import url, include


from .views import ItemView, CategoryView

urlpatterns = [
    url(r'^items/$', ItemView.as_view(), name='items'),
    url(r'^items/(?P<shop_id>\d+)/$', ItemView.as_view(), name='shop-items'),
    url(r'^categories/$', CategoryView.as_view(), name='categories'),
]
