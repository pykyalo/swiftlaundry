# ----------------package imports -----------------
from slugify import slugify

# ----------------model imports--------------------
from .models import Item, Category

# ----------------serializer imports---------------
from .serializers import (
LeanItemSerializer,
ItemSerializer,
CategorySerializer
)

# ----------------drf imports ---------------------
from rest_framework.views import  APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status


class CategoryView(APIView):
    """
        DESCRIPTION | Returns an all laundry items categories created by swift
                    | No auth required
    """

    def get(self, request, format=None):
        categories = Category.objects.all()
        categories_serializer = CategorySerializer(categories, many=True)
        return Response(categories_serializer.data, status=status.HTTP_200_OK)

class ItemView(APIView):
    """
        DESCRIPTION | Returns an item object created by outlet staff
                    | Must be authenticated

        POST {
            "title": "Shirt",
            "category_id": 1,
            "price":  200,
            "outlet_id": 1
            }
    """

    permission_classes = [IsAuthenticated, ]

    def get(self, request, shop_id, format=None):
        item_qs = Item.objects.filter(outlet_id=shop_id)
        serializer = ItemSerializer(item_qs, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


    def post(self, request, format=None):
        category_id = request.data["category_id"]
        outlet_id = request.data["outlet_id"]
        title = request.data["title"].lower()

        data = {
            "category": category_id,
            "outlet": outlet_id,
            "price": request.data["price"],
            "title": title,
            "slug": slugify(title),
            "is_active": True
        }

        # Check if outlet had created this item before
        item_qs = Item.objects.filter(
            category_id=category_id,
            outlet_id=outlet_id,
            title=title
        )

        if item_qs.exists():

            # A user may be creating an existing item
            # Just return ok instead of en error
            item_obj = item_qs.first()
            item_serializer = ItemSerializer(item_obj)

            return Response(item_serializer.data, status=status.HTTP_200_OK)

        else:
            item_serializer = LeanItemSerializer(
                data=data
            )

            if item_serializer.is_valid():
                item_serializer.save()
                return Response(
                    item_serializer.data,
                    status=status.HTTP_201_CREATED
                )
            else:
                return Response(
                    item_serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
                )
