from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.reverse import reverse


class RootView(APIView):
    def get(self, request, format=None):
        return Response({
            # Auth urls
            "getlogintoken": reverse('getlogintoken', request=request),
            "signup": reverse('signup', request=request),
            "login": reverse('login', request=request),

            # Outlet urls
            "outlet": reverse('outlet', request=request),
            "outletstaff": reverse('outletstaff', request=request),
            # "outletstaff details": reverse('outletstaff-details', request=request),
            "cities": reverse('cities', request=request),
            "titles": reverse('titles', request=request),
            "orders": reverse('outlet-orders', request=request),
            "payments": reverse('outlet-payments', request=request),

            # Order urls
            "customer": reverse('customer', request=request),
            "order": reverse('order', request=request),
            "order assignment": reverse("order-assignment", request=request),

            # Stockrecord urls
            "items": reverse('items', request=request),
            "categories": reverse('categories', request=request),


            # Payment urls
            "stk transaction": reverse('stk-transaction', request=request),
            "payment transaction": reverse('payment-transaction', request=request),
        })
